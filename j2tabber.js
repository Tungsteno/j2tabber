﻿/*! J2Tabber - v0.6.2 Beta - 16/07/2015 
* Widget for JQuery UI
* Includes: fxtrex.js
* Copyright 2015 tungsteno74@yahoo.it by Tungsteno; Licensed MIT */
(function ($, undefined) {
    $.widget("ui.tabs", $.ui.tabs, {
        options: {
            j2tabber: {
                enable: true,
                // bottomNav: "ignore", //(ignore,delete,default) bottom tab if enable is false.
                animate: true,
                duration: 1000, //or {forward: 500, rewind:500}
                ui: "j2t" // (default, j2t)
            }
        },
        _j2tui: function (thema) {
            //Theme selection of one of the standard ui (default , j2t) or a custom ui string choosen by the user.    
            if (thema != 'default') {
                this.element.addClass("ui-" + thema);
            }
            else {
                this.element.removeClass("ui-" + lastthema);
            }
            this.thema = thema;
            lastthema = thema;
        },
        _j2tanimate: function (animato) {
            //Enable or disable the Nav tab animation.
            var tranTabClsPref = "j2t-effects-transfer-",
                clspos = { top: "top", bot: "bottom" };
            if (animato) {
                //transfer effect               
                this.animate_handler = this._on({ "tabsbeforeactivate": function (event, ui) {
                    //Attach the animation function to the event handler that is always called before the active state of the Tabs widget.

                    function tabanim(tab, toTabclass, duration) {
                        //execute the transfer effect from the old tab to the new tab and apply the animation image correctly. 
                        tab.ot.css("background-image", "none");
                        tab.nt.css("background-image", "none");
                        tab.ot.find(".ui-tabs-anchor").stop(true, true).effect("transfer", { to: tab.nt.find(".ui-tabs-anchor"), className: toTabclass, trackAt: "stCoordT" }, duration, function () {
                            tab.ot.css("background-image", "");
                            tab.nt.css("background-image", "");
                        }); //fix: extend the transfer effect by changing the animation function.
                    };
                    function tabselect(tab, nm) {
                        //select the tab element (the old tab or the new selected) and return an object for easy manipulation.
                        return { nt: tab.find(">li").eq(nm.nt), ot: tab.find(">li").eq(nm.ot) };
                    };
                    var num = { ot: ui.oldTab.index(), nt: ui.newTab.index() };
                    var ta = { top: this.tablist.eq(0), bot: this.tablist.eq(1) };

                    tabselect(ta.top, num).ot.find(".ui-tabs-anchor").stop(true, true);
                    tabselect(ta.bot, num).ot.find(".ui-tabs-anchor").stop(true, true);
                    //upper tab navigation animation.  
                    tabanim(tabselect(ta.top, num), tranTabClsPref + clspos.top, this.options.j2tabber.duration);
                    //bottom tab navigation animation.
                    tabanim(tabselect(ta.bot, num), tranTabClsPref + clspos.bot, this.options.j2tabber.duration);
                }
                });
                //--------------
                this.animate = true;
            } else {
                //disable the animation handler attached to the "tabsbeforeactive" event.
                this._off(this.element, this.animate_handler);
                this.animate = false;
            }
        },
        _setOption: function (key, value) {
            if (this.options.j2tabber.enable) {
                if (key === "j2tabber") {
                    this._j2tui(value.ui);
                    this._j2tanimate(value.animate);
                }
            }
            return this._super(key, value);
        },
        _create: function () {
            var sup = this._super();
            if (this.options.j2tabber.enable) {
                this._j2tui(this.options.j2tabber.ui);
                this._j2tanimate(this.options.j2tabber.animate);
            }
            return sup;
        },
        _getList: function () {
            if (this.options.j2tabber.enable) {
                var list = this.element.find("ul,ol");
                return list.length ? list : this._super();
            } else {
                return this._super();
            }
        },
        //overwritten processtab, copied from the original and add the code for bottom tabs creation.
        _processTabs: function () {
            if (this.options.j2tabber.enable) {
                var that = this,
                	prevTabs = this.tabs,
			        prevAnchors = this.anchors,
			        prevPanels = this.panels,
                    mainTablist, slaveTablist, ordAnchors, anchorsel, uplist, downlist, viewlist;

                this.tablist = this._getList()
			    .addClass("ui-tabs-nav ui-helper-reset ui-helper-clearfix ui-widget-header ui-corner-all")
			    .attr("role", "tablist")

                // Prevent users from focusing disabled tabs via click
			    .delegate("> li", "mousedown" + this.eventNamespace, function (event) {
			        if ($(this).is(".ui-state-disabled")) {
			            event.preventDefault();
			        }
			    })

                // support: IE <9
                // Preventing the default action in mousedown doesn't prevent IE
                // from focusing the element, so if the anchor gets focused, blur.
                // We don't have to worry about focusing the previously focused
                // element since clicking on a non-focusable element should focus
                // the body anyway.
			    .delegate(".ui-tabs-anchor", "focus" + this.eventNamespace, function () {
			        if ($(this).closest("li").is(".ui-state-disabled")) {
			            this.blur();
			        }
			    });
                this.tablist.eq(0).addClass("ui-nav-top");
                this.tablist.eq(1).addClass("ui-nav-bottom");
                this.tabs = this.tablist.find("> li:has(a[href])")
			    .addClass("ui-state-default ui-corner-top")
			    .attr({
			        role: "tab",
			        tabIndex: -1
			    });
                this.anchors = this.tabs.map(function () {
                    return $("a", this)[0];
                })
			    .addClass("ui-tabs-anchor")
			    .attr({
			        role: "presentation",
			        tabIndex: -1
			    });

                this.panels = $();

                uplist = this.tablist.eq(0).find("> li:has(a[href])"),
                downlist = this.tablist.eq(1).find("> li:has(a[href])");

                if (uplist.length < downlist.length) {
                    mainTablist = downlist;
                    slaveTablist = uplist;
                    ordAnchors = $(this.anchors.get().reverse());
                    viewlist = function (i) {
                        return that.tablist.eq(0);
                    }
                    anchorsel = function (i) {
                        return ordAnchors.eq((i - mainTablist.length) + (mainTablist.length - slaveTablist.length));
                    };

                } else {
                    mainTablist = uplist;
                    slaveTablist = downlist;
                    ordAnchors = this.anchors;
                    viewlist = function (i) {
                        return that.panels[i - 1] || that.tablist.eq(0);
                    }
                    anchorsel = function (i) {
                        return ordAnchors.eq(i - mainTablist.length);
                    };
                }

                ordAnchors.each(function (i, anchor) {
                    var selector, panel, panelId,
				        anchorId = $(anchor).uniqueId().attr("id"),
				        tab = $(anchor).closest("li"),
				        originalAriaControls = tab.attr("aria-controls");

                    // inline tab
                    if (that._isLocal(anchor)) {
                        selector = anchor.hash;
                        panelId = selector.substring(1);
                        panel = that.element.find(that._sanitizeSelector(selector));
                        // remote tab
                    } else {
                        // If the tab doesn't already have aria-controls,
                        // generate an id by using a throw-away element

                        if (mainTablist.has(anchor).length == 1) {
                            //main tab and panel creation
                            panelId = tab.attr("aria-controls") || $({}).uniqueId()[0].id;
                            selector = "#" + panelId;
                            panel = that.element.find(selector);
                            if (!panel.length) {
                                panel = that._createPanel(panelId);
                                panel.insertAfter(viewlist(i));
                            }
                            panel.attr("aria-live", "polite");
                        } else {
                            //link to the main tab by slave tab.
                            panelId = tab.attr("aria-controls") || anchorsel(i).closest("li").attr("aria-controls");
                            panel = $();
                        }
                    }

                    if (panel.length) {
                        that.panels = that.panels.add(panel);
                    }
                    if (originalAriaControls) {
                        tab.data("ui-tabs-aria-controls", originalAriaControls);
                    }
                    tab.attr({
                        "aria-controls": panelId,
                        "aria-labelledby": anchorId
                    });
                    panel.attr("aria-labelledby", anchorId);
                });

                this.panels
			    .addClass("ui-tabs-panel ui-widget-content ui-corner-bottom")
			    .attr("role", "tabpanel");

                // Avoid memory leaks (#10056)
                if (prevTabs) {
                    this._off(prevTabs.not(this.tabs));
                    this._off(prevAnchors.not(this.anchors));
                    this._off(prevPanels.not(this.panels));
                }
            } else {
                return this._super();
            }
        },
        //overwritten destroy, copied from the original and add the code for bottom tabs deletion.
        _destroy: function () {
            if (this.options.j2tabber.enable) {
                this.tablist.eq(0).removeClass("ui-nav-top");
                this.tablist.eq(1).removeClass("ui-nav-bottom");
                return this._super();
            } else {
                return this._super();
            }
        },
        //overwritten refresh, copied from the original and add the code for bottom tabs activation.
        _refresh: function () {
            function classattr(obj) {
                obj.addClass("ui-tabs-active ui-state-active").attr({
                    "aria-selected": "true",
                    "aria-expanded": "true",
                    tabIndex: 0
                });
            }
            if (this.options.j2tabber.enable) {
                this._setupDisabled(this.options.disabled);
                this._setupEvents(this.options.event);
                this._setupHeightStyle(this.options.heightStyle);
                var botlist = this.tablist.eq(1).find(">li");

                this.tabs.not(this.active).attr({
                    "aria-selected": "false",
                    "aria-expanded": "false",
                    tabIndex: -1
                });
                this.panels.not(this._getPanelForTab(this.active))
			    .hide()
			    .attr({
			        "aria-hidden": "true"
			    });

                // Make sure one tab is in the tab order
                if (!this.active.length) {
                    this.tabs.eq(0).attr("tabIndex", 0);
                    botlist.eq(0).attr("tabIndex", 0);
                } else {
                    classattr(this.active);
                    classattr(botlist.eq(this.active.index()));
                    this._getPanelForTab(this.active)
				    .show()
				    .attr({
				        "aria-hidden": "false"
				    });
                }
            } else {
                return this._super();
            }
        },
        //overwritten toggle, copied from the original and add the code for bottom tabs activation.
        _toggle: function (event, eventData) {
            var that = this;
            function complete() {
                that.running = false;
                that._trigger("activate", event, eventData);
            }
            function show(evD, nN) {
                evD.newTab.closest("li").addClass("ui-tabs-active ui-state-active");
                nN.addClass("ui-tabs-active ui-state-active");
                if (toShow.length && that.options.show) {
                    that._show(toShow, that.options.show, complete);
                } else {
                    toShow.show();
                    complete();
                }
            }
            if (this.options.j2tabber.enable) {
                var navNew, navOld,
			    toShow = eventData.newPanel,
			    toHide = eventData.oldPanel;

                if (eventData.newTab.closest("li").is(this.tablist.eq(0).find(">li").eq(eventData.newTab.closest("li").index()))) {
                    navNew = this.tablist.eq(1).find(">li").eq(eventData.newTab.closest("li").index());
                } else {
                    navNew = this.tablist.eq(0).find(">li").eq(eventData.newTab.closest("li").index());
                }
                if (eventData.oldTab.closest("li").is(this.element.find(">ul:first,>ol:first").find(">li").eq(eventData.oldTab.closest("li").index()))) {
                    navOld = this.tablist.eq(1).find(">li").eq(eventData.oldTab.closest("li").index());
                } else {
                    navOld = this.tablist.eq(0).find(">li").eq(eventData.oldTab.closest("li").index());
                }
                this.running = true;
                var ariaatt = {
                    tru: function (obj) {
                        obj.attr({
                            "aria-selected": "true",
                            "aria-expanded": "true",
                            tabIndex: 0
                        });
                    },
                    fal: function (obj) {
                        obj.attr({
                            "aria-selected": "false",
                            "aria-expanded": "false"
                        });
                    }
                };
                // start out by hiding, then showing, then completing
                if (toHide.length && this.options.hide) {
                    this._hide(toHide, this.options.hide, function () {
                        eventData.oldTab.closest("li").removeClass("ui-tabs-active ui-state-active");
                        navOld.removeClass("ui-tabs-active ui-state-active");
                        show(eventData, navNew);
                    });
                } else {
                    eventData.oldTab.closest("li").removeClass("ui-tabs-active ui-state-active");
                    navOld.removeClass("ui-tabs-active ui-state-active");
                    toHide.hide();
                    show(eventData, navNew);
                }

                toHide.attr("aria-hidden", "true");
                ariaatt.fal(eventData.oldTab);
                ariaatt.fal(navOld);
                // If we're switching tabs, remove the old tab from the tab order.
                // If we're opening from collapsed state, remove the previous tab from the tab order.
                // If we're collapsing, then keep the collapsing tab in the tab order.
                if (toShow.length && toHide.length) {
                    eventData.oldTab.attr("tabIndex", -1);
                    navOld.attr("tabIndex", -1);
                } else if (toShow.length) {
                    this.tabs.filter(function () {
                        return $(this).attr("tabIndex") === 0;
                    }).attr("tabIndex", -1);
                }
                toShow.attr("aria-hidden", "false");
                ariaatt.tru(eventData.newTab);
                ariaatt.tru(navOld);
            } else {
                return this._super();
            }
        }
    });

    /*
    || JQuery UI Transfer Effect Extension by Valerio Baldereschi alias Tungsteno
    ||
    || This effect basically is similar to the default Transfer effect but update the start and end points at animation execution,
    || thus you can use it with dinamics objects.
    */
    var effectTransfer = $.effects.effect.transfer = function (o, done) {
        var steps = 0;
        var elem = $(this),
		target = $(o.to),
		targetFixed = target.css("position") === "fixed",
		body = $("body"),
		fixTop = targetFixed ? body.scrollTop() : 0,
		fixLeft = targetFixed ? body.scrollLeft() : 0,
		endPosition = target.offset(),
		animation = {
		    top: endPosition.top - fixTop,
		    left: endPosition.left - fixLeft,
		    height: target.innerHeight(),
		    width: target.innerWidth()
		},
		startPosition = elem.offset(),
        trackat = o.trackAt === undefined || "" ? "none" : o.trackAt,
        trackset = (function (trType) {
            //choose the track type.
            var fnalg = [], indx, indxfn,
                arraysel = trType.split(" ");
            for (indx = 0; indx < arraysel.length; ++indx) {
                switch (arraysel[indx]) {
                    case "none":
                        fnalg[indx] = function (prm) { }; //dummy algorithm for none
                        break;
                    case "stObjT":
                        fnalg[indx] = function (prm) { //algorithm function for start and move by top.
                            if (prm.fx.prop === "top") {
                                var startelem = elem,
                                targetFixed = target.css("position") === "fixed",
                                fix = targetFixed ? body.scrollTop() : 0,
                                step = startelem.offset().top - fix;
                                prm.fx.start = step;
                            }
                        };
                        break;
                    case "stObjL":
                        fnalg[indx] = function (prm) { //algorithm function for start and move by left.
                            if (prm.fx.prop === "left") {
                                var startelem = elem,
                                targetFixed = target.css("position") === "fixed",
                                fix = targetFixed ? body.scrollLeft() : 0,
                                step = startelem.offset().left - fix;
                                prm.fx.start = step;
                            }
                        };
                        break;
                    case "enObjT":
                        fnalg[indx] = function (prm) { //algorithm function for end.
                            if (prm.fx.prop === "top") {
                                var targetelem = target,
                                targetFixed = target.css("position") === "fixed",
                                fix = targetFixed ? body.scrollTop() : 0,
                                step = targetelem.offset().top - fix;
                                prm.fx.end = step;
                            }
                        };
                        break;
                    case "enObjL":
                        fnalg[indx] = function (prm) { //algorithm function for end.
                            if (prm.fx.prop === "left") {
                                var targetelem = target,
                                targetFixed = target.css("position") === "fixed",
                                fix = targetFixed ? body.scrollLeft() : 0,
                                step = targetelem.offset().left - fix;
                                prm.fx.end = step;
                            }
                        };
                        break;
                    case "stCoordT":
                        fnalg[indx] = function (prm) { //algorithm function for follow X coordinate of the start object.
                            if (prm.fx.prop === "top") {
                                var startelem = elem,
                                targetFixed = target.css("position") === "fixed",
                                fix = targetFixed ? body.scrollTop() : 0,
                                step = startelem.offset().top - fix;
                                prm.fx.now = step;
                            }
                        };
                        break;
                    case "stCoordL":
                        fnalg[indx] = function (prm) { //algorithm function for follow Y coordinate of the start object.
                            if (prm.fx.prop === "left") {
                                var startelem = elem,
                                targetFixed = target.css("position") === "fixed",
                                fix = targetFixed ? body.scrollLeft() : 0,
                                step = startelem.offset().left - fix;
                                prm.fx.now = step;
                            }
                        };
                        break;
                    case "enCoordT":
                        fnalg[indx] = function (prm) { //algorithm function for follow X position of the end object.
                            if (prm.fx.prop === "top") {
                                var targetelem = target,
                                targetFixed = target.css("position") === "fixed",
                                fix = targetFixed ? body.scrollTop() : 0,
                                step = targetelem.offset().top - fix;
                                prm.fx.now = step;
                            }
                        };
                        break;
                    case "enCoordL":
                        //TODO: Problem when the object move left to right or vice versa;
                        fnalg[indx] = function (prm) { //algorithm function for follow Y position of the end object.
                            if (prm.fx.prop === "left") {
                                var targetelem = target,
                                targetFixed = target.css("position") === "fixed",
                                fix = targetFixed ? body.scrollLeft() : 0,
                                step = targetelem.offset().left - fix;
                                prm.fx.now = step;
                            }
                        };
                        break;
                    default:
                        fnalg[indx] = null; //here need raise an exception and next some try catch event, for all wrong strings. 
                };
            };
            return fnalg;
        })(trackat),
        lastPosition = { top: startPosition.top - fixTop, left: startPosition.left - fixLeft },
        transfer = $("<div class='ui-effects-transfer'></div>")
			.appendTo(document.body)
			.addClass(o.className)
			.css({
			    top: lastPosition.top,
			    left: lastPosition.left,
			    height: elem.innerHeight(),
			    width: elem.innerWidth(),
			    position: targetFixed ? "fixed" : "absolute"
			})
			.animate(animation, {
			    duration: o.duration,
			    easing: o.easing,
			    step: function (now, fx) {
			        for (indx = 0; indx < trackset.length; ++indx)
			            trackset[indx]({ fx: fx }); //function alghoritm here.
			        // alert("start: " + fx.start + "|" + "now: " + fx.end + "end: " + fx.now);
			        //steps += 1;			        
			    },
			    complete: function () {
			        //alert("step:" + steps);
			        transfer.remove();
			        done();
			    }
			});
    };
})(jQuery);